<?php
use Illuminate\Http\Request;

Route::group([
    'middleware' => ['api'],
    'prefix' => 'api/v1'
], function () {
    Route::post('evaluation/get', 'Inmovsoftware\TrainingApi\Http\Controllers\V1\EvaluationController@get_evaluation');
    Route::post('evaluation/answer/save', 'Inmovsoftware\TrainingApi\Http\Controllers\V1\EvaluationController@save_answer');
    Route::get('get/{training_id}/file', 'Inmovsoftware\TrainingApi\Http\Controllers\V1\TrainingController@get_document');
});
