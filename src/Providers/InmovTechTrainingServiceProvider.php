<?php

namespace Inmovsoftware\TrainingApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\TrainingApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Artisan;

class InmovTechTrainingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'trainignLangs');

        Artisan::call('vendor:publish' , [
                      '--tag' => 'trainignLangs',
                    '--force' => true,
        ]);

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'trainignLangs');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Inmovsoftware\TrainingApi\Models\V1\Evaluation');
        $this->app->make('Inmovsoftware\TrainingApi\Http\Controllers\V1\EvaluationController');
        $this->app->make('Inmovsoftware\TrainingApi\Models\V1\Training');
        $this->app->make('Inmovsoftware\TrainingApi\Http\Controllers\V1\TrainingController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
