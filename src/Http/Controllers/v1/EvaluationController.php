<?php
namespace Inmovsoftware\TrainingApi\Http\Controllers\V1;

use Inmovsoftware\TrainingApi\Http\Resources\V1\GlobalCollection;
use Inmovsoftware\TrainingApi\Models\V1\Evaluation;
use Inmovsoftware\TrainingApi\Models\V1\Training;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Redirect;
use Log;
use DB;

class EvaluationController extends Controller
{

    public function get_evaluation(Request $request)
    {
        $data = $request->validate([
            "id" => "required|exists:vs_login,id",
        ]);

        $evaluation = DB::table('evaluation')->select('id', 'name', 'intro', 'publish_up', 'publish_down')
            ->where('status', '=', 'A')
            ->get();


        $count_file = DB::table('evaluation_training')
        ->select(DB::raw('count(*) as count'))
        ->where('evaluation_id', '=', $evaluation[0]->id)
        ->where('status', '=', 'A')
        ->get();
        $count_file = $count_file[0]->count;

        $count_taked = DB::table('evaluation_training_progress')
        ->select(DB::raw('count(evaluation_training_progress.id) as count'))
        ->join('evaluation_training', 'evaluation_training_progress.evaluation_training_id', '=', 'evaluation_training.id')
        ->where('evaluation_training_progress.users_id', '=', $data["id"])
        ->where('evaluation_training.evaluation_id', '=', $evaluation[0]->id)
        ->where('evaluation_training.status', '=', 'A')
        ->get();
        $count_taked = $count_taked[0]->count;

        if($count_file != $count_taked){
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => ["Debes leer todo el material de apoyo antes de tomar la evaluación."]
                    ]
                ],
                401
            );
            exit();
        }

     if(count($evaluation) > 0 && $evaluation[0]->id > 0){

        $count = DB::table('evaluation_response')
        ->select(DB::raw('count(*) as count'))
        ->where('status', '=', 'A')
        ->where('evaluation_id', '=', $evaluation[0]->id)
        ->where('user_id', '=', $data["id"])
        ->get();

        $is_approved = $count[0]->count;
        if($is_approved > 0){

            $responses = DB::table('evaluation_response')
            ->select('*')
            ->where('status', '=', 'A')
            ->where('evaluation_id', '=', $evaluation[0]->id)
            ->where('user_id', '=',  $data["id"])
            ->get();

            $count = 0;
            foreach($responses AS  $hey => $response_){

                $countYes = DB::table('vs_evaluation_answers_full')->select(DB::raw('count(*) as count'))
                ->where('user_id', '=', $data["id"])
                ->where('evaluation_id', '=', $evaluation[0]->id)
                ->where('response_id', '=', $response_->id)
                ->where('response_status', '!=', 'C')
                ->where('correct', '=', 'Si')
                ->get();

                $countNo = DB::table('vs_evaluation_answers_full')->select(DB::raw('count(*) as count'))
                ->where('user_id', '=', $data["id"])
                ->where('evaluation_id', '=', $evaluation[0]->id)
                ->where('response_id', '=', $response_->id)
                ->where('response_status', '!=', 'C')
                ->where('correct', '=', 'No')
                ->get();

                $result["attempt"][$count]['correct'] = $countYes[0]->count;
                $result["attempt"][$count]['incorrect'] = $countNo[0]->count;
                $result["attempt"][$count]['id'] = $response_->id;
                $result["attempt"][$count]['status'] = $response_->status;
                $result["attempt"][$count]['date'] = $response_->date;
                $count++;
            }


            return response()->json(
                [
                    'errors' => [
                        'status' => 200,
                            'messages' => ["El usuario ya ha aprobado esta evaluación."],
                            'type' => "approved",
                            'results' => $result
                    ]
                ],
                200
            );
            exit();
        }


        $count = DB::table('evaluation_response')
        ->select(DB::raw('count(*) as count'))
        ->where('status', '!=', 'C')
        ->where('evaluation_id', '=', $evaluation[0]->id)
        ->where('user_id', '=', $data["id"])
        ->get();

        $total_attempts = $count[0]->count;
        if ($total_attempts < 3) {

            $return_object = array();

            $return_object["quiz"]["id"] = $evaluation[0]->id;
            $return_object["quiz"]["quizTitle"] = $evaluation[0]->name;
            $return_object["quiz"]["quizSynopsis"] = $evaluation[0]->intro;
            $return_object["publish_up"] = $evaluation[0]->publish_up;
            $return_object["publish_down"] = $evaluation[0]->publish_down;

            $questions = DB::table('evaluation_question')->select('id', 'evaluation_question_type_id', 'question', 'extra_info', 'order', 'required')
                ->where('evaluation_id', '=', $evaluation[0]->id)
                ->where('status', '=', 'A')
                ->orderBy('order', 'asc')
                ->get();

            $cnt = 0;
            foreach ($questions as $key => $values) {

                $correct = DB::table('evaluation_parameters')->select('id', "name", "order")
                    ->where('evaluation_question_id', '=', $values->id)
                    ->where('correct', '=', 'true')
                    ->orderBy('status', 'A')
                    ->get();

                $return_object["quiz"]["questions"][$cnt]["id"] = $values->id;
                $return_object["quiz"]["questions"][$cnt]["question"] = $values->question;
                $return_object["quiz"]["questions"][$cnt]["questionType"] = "text";
                $return_object["quiz"]["questions"][$cnt]["messageForCorrectAnswer"] = "Respuesta Correcta.";
                $return_object["quiz"]["questions"][$cnt]["messageForIncorrectAnswer"] = "Respuesta Incorrecta.";
                $return_object["quiz"]["questions"][$cnt]["explanation"] = "Selecciona la respuesta correcta.";
                $return_object["quiz"]["questions"][$cnt]["correctAnswer"] = $correct[0]->order;


                $answers = DB::table('evaluation_parameters')->select('id', 'name', 'correct')
                    ->where('evaluation_question_id', '=', $values->id)
                    ->where('status', '=', 'A')
                    ->orderBy('order', 'asc')
                    ->get();

                $an = 0;
                foreach ($answers as $key2 => $values2) {
                    $return_object["quiz"]["questions"][$cnt]["answers"][$an]["id"] = $values2->id;
                    $return_object["quiz"]["questions"][$cnt]["answers"][$an]["answer"] = $values2->name;
                    $return_object["quiz"]["questions"][$cnt]["answers"][$an]["correct"] = $values2->correct;
                    $an++;
                }

                $cnt++;
            }

            $return_object["attempts_left"] = 3 - $total_attempts;
            return response()->json($return_object);
        } else {

            $responses = DB::table('evaluation_response')
            ->select('*')
            ->where('status', '!=', 'C')
            ->where('evaluation_id', '=', $evaluation[0]->id)
            ->where('user_id', '=',  $data["id"])
            ->get();

            $count = 0;
            foreach($responses AS  $hey => $response_){

                $countYes = DB::table('vs_evaluation_answers_full')->select(DB::raw('count(*) as count'))
                ->where('user_id', '=', $data["id"])
                ->where('evaluation_id', '=', $evaluation[0]->id)
                ->where('response_id', '=', $response_->id)
                ->where('response_status', '!=', 'C')
                ->where('correct', '=', 'Si')
                ->get();

                $countNo = DB::table('vs_evaluation_answers_full')->select(DB::raw('count(*) as count'))
                ->where('user_id', '=', $data["id"])
                ->where('evaluation_id', '=', $evaluation[0]->id)
                ->where('response_id', '=', $response_->id)
                ->where('response_status', '!=', 'C')
                ->where('correct', '=', 'No')
                ->get();

                $result["attempt"][$count]['correct'] = $countYes[0]->count;
                $result["attempt"][$count]['incorrect'] = $countNo[0]->count;
                $result["attempt"][$count]['id'] = $response_->id;
                $result["attempt"][$count]['status'] = $response_->status;
                $result["attempt"][$count]['date'] = $response_->date;
                $count++;
            }
                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'type' => "not approved",
                            'messages' => ["El usuario ya ha contestado esta evaluación en 3 ocasiones"],
                            'results' => $result
                            ]
                        ],
                        401
                    );
                    exit();
                }

            }else{

                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => ["No se ha encontrado ninguna evaluación activa"]
                            ]
                        ],
                        401
                    );
                    exit();
                }


    }

    public function save_answer(Request $request)
    {
        $data = $request->validate([
            "user_id" => "required|exists:vs_login,id",
            "evaluation_id" => "required|exists:evaluation,id",
            "responses" => "required"
        ]);

        $responses = $request->only(['responses']);

        $count = DB::table('evaluation_response')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status', '!=', 'C')
                    ->where('evaluation_id', '=', $data["evaluation_id"])
                    ->where('user_id', '=', $data["user_id"])
                    ->get();

        $total_count = $count[0]->count;

        if ($total_count >= 3) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => ["El usuario ya ha realizado 3 veces esta evaluación."]
                    ]
                ],
                401
            );
            exit();
        }



        $idResponse = DB::table('evaluation_response')->insertGetId(
            [
                'user_id' => $data["user_id"],
                'evaluation_id' => $data["evaluation_id"],
                //'status' => 'A',
                'date' => Carbon::now(),
                'created_at' => Carbon::now()
                ]
        );

        $aproved = 0;
        $reproved = 0;
        $counter = 0;
        foreach ($responses['responses']  AS $index => $content) {

                /*
                $count = DB::table('evaluation_answers')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status', '=', 'A')
                    ->where('evaluation_id', '=', $data["evaluation_id"])
                    ->where('evaluation_question_id', '=', $content["question_id"])
                    ->where('user_id', '=', $data["user_id"])
                    ->get();

                if ($count[0]->count < 1) {*/

                $selected = DB::table('evaluation_parameters')
                    ->select('name', 'value', 'correct')
                    ->where('id', '=',  $content["selected_id"])
                    ->get();

                DB::table('evaluation_answers')->insert(
                    [
                        'user_id' => $data["user_id"],
                        'evaluation_id' => $data["evaluation_id"],
                        'evaluation_question_id' => $content["question_id"],
                        'evaluation_response_id' => $idResponse,
                        'selected_id' => $content["selected_id"],
                        'answer_text' => $selected[0]->name . ' - ' . $selected[0]->value . '(' . $selected[0]->correct . ')',
                        'status' => 'A'
                    ]
                );
                $counter++;
                if($selected[0]->correct == "true"){
                    $aproved++;
                }else{
                    $reproved++;
                }

            /*} else {

                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => ["El usuario ya ha contestado esta pregunta"]
                        ]
                    ],
                    401
                );
            }*/
        }

        $aproved_reproved = (($aproved*100)/$counter);
        $reproved_reproved = (($reproved*100)/$counter);
        if($aproved_reproved >= 80){
            $status = 'A';
        }else{
            $status = 'R';
        }

        DB::table('evaluation_response')
            ->where('id', $idResponse)
            ->update(['status' => $status]);

        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'messages' => ["Evaluación guardada con éxito"],
                    'evaluation_status' => $status,
                    'results' => [
                        "Aproved"=> $aproved,
                        "Aproved_percent"=> $aproved_reproved,
                        "Reproved"=> $reproved,
                        "Reproved_percent"=> $reproved_reproved,
                        "Attempts" => $total_count+1
                    ]
                ]
            ],
            200
        );

    }


}
