<?php
namespace Inmovsoftware\TrainingApi\Http\Controllers\V1;

use Inmovsoftware\TrainingApi\Http\Resources\V1\GlobalCollection;
use Inmovsoftware\TrainingApi\Models\V1\Evaluation;
use Inmovsoftware\TrainingApi\Models\V1\Training;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TrainingController extends Controller
{

    public function get_document($training_id, Request $request)
    {
        $data = $request->validate([
            "id" => "required|exists:vs_login,id",
            "url" => "required",
        ]);


            $count_file = DB::table('evaluation_training')
            ->select(DB::raw('count(*) as count'))
            ->where('id', '=', $training_id)
            ->where('status', '=', 'A')
            ->get();

            $count_file = $count_file[0]->count;

        if($count_file == 1){

            $count = DB::table('evaluation_training_progress')
            ->select(DB::raw('count(*) as count'))
            ->where('evaluation_training_id', '=', $training_id)
            ->where('users_id', '=', $data["id"])
            ->get();

            $all_ready_seen = $count[0]->count;
            if($all_ready_seen < 1){
                DB::table('evaluation_training_progress')->insert(
                    [
                        'users_id' => $data["id"],
                        'evaluation_training_id' => $training_id
                    ]
                );
            }

                        $evaluation = DB::table('evaluation_training')
                        ->select('*')
                        ->where('id', '=', $training_id)
                        ->get();

            return Redirect::to( $data["url"].'/'.$evaluation[0]->file);



        }else{

            return Redirect::to($data["url"].'/file_not_exits');
        }





    }

}
