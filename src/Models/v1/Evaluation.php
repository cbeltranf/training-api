<?php

namespace Inmovsoftware\TrainingApi\Models\V1;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    use SoftDeletes;
    protected $table = "evaluation";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['name','intro','status','created_by','publish_up','publish_down','percent','created_at'];


}
