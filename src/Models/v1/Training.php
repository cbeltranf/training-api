<?php

namespace Inmovsoftware\TrainingApi\Models\V1;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use SoftDeletes;
    protected $table = "evaluation_training";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['file','evaluation_id','status'];
}
